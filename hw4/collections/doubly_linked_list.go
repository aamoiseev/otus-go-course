package collections

// Item is a container for value in doubly linked list.
type Item struct {
	prev  *Item
	next  *Item
	value interface{}
}

// Prev returns previous Item.
func (i *Item) Prev() *Item {
	return i.prev
}

// Next returns next Item.
func (i *Item) Next() *Item {
	return i.next
}

// Value returns current Item value.
func (i *Item) Value() interface{} {
	return i.value
}

// List represents doubly linked list.
type List struct {
	first *Item
	last  *Item
	len   int
}

// NewList creates new doubly linked list.
func NewList() *List {
	return &List{}
}

// Len returns current length of doubly linked list.
func (l *List) Len() int {
	return l.len
}

// First returns first Item of doubly linked list.
func (l *List) First() *Item {
	return l.first
}

// Last returns last Item of doubly linked list.
func (l *List) Last() *Item {
	return l.last
}

// PushFront add Item to front of doubly linked list.
func (l *List) PushFront(v interface{}) {
	first := l.first

	item := &Item{
		next:  first,
		value: v,
	}

	if first != nil {
		first.prev = item
	}

	l.first = item

	if l.last == nil {
		l.last = item
	}

	l.len++
}

// PushBack add Item to back of doubly linked list.
func (l *List) PushBack(v interface{}) {
	last := l.last

	item := &Item{
		prev:  last,
		value: v,
	}

	if last != nil {
		last.next = item
	}

	l.last = item

	if l.first == nil {
		l.first = item
	}

	l.len++
}

// Remove removes Item from doubly linked list.
func (l *List) Remove(item *Item) {
	current := l.first

	for current != nil {
		if *current == *item {
			prev := current.prev
			next := current.next

			if prev != nil {
				prev.next = next
			}

			if next != nil {
				next.prev = prev
			}

			if *l.first == *item {
				l.first = next
			}

			if *l.last == *item {
				l.last = prev
			}

			l.len--

			break
		}

		current = current.next
	}
}

// Reverse reverses doubly linked list.
func (l *List) Reverse() {
	first := l.first
	last := l.last

	current := first
	for current != nil {
		prev := current.prev
		next := current.next

		current.prev = current
		current.next = prev

		current = next
	}

	l.first = last
	l.last = first
}

// Each iterate over doubly linked list and call cb on every Item.
func (l *List) Each(cb func(*Item)) {
	current := l.first
	for current != nil {
		cb(current)

		current = current.Next()
	}
}
