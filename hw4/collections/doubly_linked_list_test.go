package collections

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var tests = [][]interface{}{
	{10, 15, 20},
	{"last", "first"},
	{"last", -14.14, 50, "first"},
	{0.0},
}

func TestNewList(t *testing.T) {
	list := NewList()

	assert.Equal(t, list.Len(), 0)
	assert.Nil(t, list.First())
	assert.Nil(t, list.Last())
}

func TestList_PushFront(t *testing.T) {
	var middle *Item

	for _, values := range tests {
		list := NewList()

		for i, v := range values {
			list.PushFront(v)

			first := list.First()
			last := list.Last()

			assert.NotNil(t, first)
			assert.NotNil(t, last)
			assert.Equal(t, list.Len(), i+1)
			assert.Equal(t, v, first.Value())

			start := i == 0
			if start {
				assert.Nil(t, first.Prev())
				assert.Nil(t, first.Next())

				assert.Equal(t, first, last)
			}

			end := i == len(values)-1
			if end {
				assert.Nil(t, first.Prev())

				if i > 1 {
					assert.NotNil(t, first.Next())
				}

				if i > 2 {
					assert.NotEqual(t, first, last)
				}
			}

			if middle != nil {
				assert.NotNil(t, middle.Prev())
				assert.NotNil(t, middle.Next())
				assert.Equal(t, v, middle.Prev().Value())
				assert.Equal(t, values[i-2], middle.Next().Value())
			}

			middle = nil
			if !start && !end {
				middle = first
			}
		}
	}
}

func TestList_PushBack(t *testing.T) {
	var middle *Item

	for _, values := range tests {
		list := NewList()

		for i, v := range values {
			list.PushBack(v)

			first := list.First()
			last := list.Last()

			assert.NotNil(t, first)
			assert.NotNil(t, last)
			assert.Equal(t, list.Len(), i+1)
			assert.Equal(t, v, last.Value())

			start := i == 0
			if start {
				assert.Nil(t, last.Prev())
				assert.Nil(t, last.Next())

				assert.Equal(t, first, last)
			}

			end := i == len(values)-1
			if end {
				assert.Nil(t, last.Next())

				if i > 1 {
					assert.NotNil(t, last.Prev())
				}

				if i > 2 {
					assert.NotEqual(t, first, last)
				}
			}

			if middle != nil {
				assert.NotNil(t, middle.Prev())
				assert.NotNil(t, middle.Next())
				assert.Equal(t, v, middle.Next().Value())
				assert.Equal(t, values[i-2], middle.Prev().Value())
			}

			middle = nil
			if !start && !end {
				middle = last
			}
		}
	}
}

func TestList_Remove_FirstItem(t *testing.T) {
	list := NewList()

	list.PushBack(100)
	list.PushBack(200)
	list.PushBack(300)

	assert.Equal(t, 3, list.Len())

	first := list.First()

	list.Remove(first)

	assert.Equal(t, 2, list.Len())

	assert.NotNil(t, list.First())
	assert.Equal(t, 200, list.First().Value())

	assert.NotNil(t, list.Last())
	assert.Equal(t, 300, list.Last().Value())
}

func TestList_Remove_LastItem(t *testing.T) {
	list := NewList()

	list.PushBack(100)
	list.PushBack(200)
	list.PushBack(300)

	assert.Equal(t, 3, list.Len())

	last := list.Last()

	list.Remove(last)

	assert.Equal(t, 2, list.Len())

	assert.NotNil(t, list.First())
	assert.Equal(t, 100, list.First().Value())

	assert.NotNil(t, list.Last())
	assert.Equal(t, 200, list.Last().Value())
}

func TestList_Remove_MiddleItem(t *testing.T) {
	list := NewList()

	list.PushBack(100)
	list.PushBack(200)

	middle := list.Last()

	list.PushBack(300)

	assert.Equal(t, 3, list.Len())

	list.Remove(middle)

	assert.Equal(t, 2, list.Len())

	assert.NotNil(t, list.First())
	assert.Equal(t, 100, list.First().Value())

	assert.NotNil(t, list.Last())
	assert.Equal(t, 300, list.Last().Value())
}

func TestList_Remove_OneItem(t *testing.T) {
	list := NewList()

	list.PushBack(100)

	assert.Equal(t, 1, list.Len())

	first := list.First()

	list.Remove(first)

	assert.Equal(t, 0, list.Len())

	assert.Nil(t, list.First())
	assert.Nil(t, list.Last())
}

func TestList_Reverse(t *testing.T) {
	for _, values := range tests {
		list := NewList()

		for _, v := range values {
			list.PushBack(v)
		}

		list.Reverse()

		current := list.First()
		i := len(values) - 1
		for current != nil {
			assert.Equal(t, values[i], current.Value())

			current = current.Next()
			i--
		}

	}
}

func TestList_ReverseEmpty(t *testing.T) {
	list := NewList()

	list.Reverse()

	assert.Equal(t, list.Len(), 0)
	assert.Nil(t, list.First())
	assert.Nil(t, list.Last())
}

func TestList_Each(t *testing.T) {
	list := NewList()

	list.PushBack(1)
	list.PushBack(2)
	list.PushBack(3)

	i := 0

	list.Each(func(item *Item) {
		i++

		assert.Equal(t, i, item.Value())
	})

	assert.Equal(t, 3, i)
}
