package main

import "gitlab.com/aamoiseev/otus-go-course/hw7/goenv"

type Cmd struct {
	goenv.EnvReader
	goenv.EnvWriter
	goenv.Executor
}

func (c *Cmd) Run(dirname string, child string, args ...string) error {
	env, err := c.Read(dirname)
	if err != nil {
		return err
	}

	err = c.Write(env)
	if err != nil {
		return err
	}

	return c.Exec(child, args...)
}
