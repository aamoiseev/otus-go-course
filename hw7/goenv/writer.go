package goenv

type EnvWriter interface {
	Write(env map[string]string) error
}

type Env struct {
	Unsetenv func(key string) error
	Setenv   func(key, value string) error
}

func (e *Env) Write(env map[string]string) error {
	for name, val := range env {
		err := e.Unsetenv(name)
		if err != nil {
			return err
		}

		err = e.Setenv(name, val)
		if err != nil {
			return err
		}
	}

	return nil
}
