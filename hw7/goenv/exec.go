package goenv

type Runner interface {
	Run() error
}

type Executor interface {
	Exec(name string, arg ...string) error
}

type Cmd struct {
	Command func(name string, arg ...string) Runner
}

func (c *Cmd) Exec(name string, arg ...string) error {
	return c.Command(name, arg...).Run()
}
