package main

import (
	"flag"
	"fmt"
	"io"
	"os"

	"github.com/cheggaaa/pb/v3"
)

var from string
var to string
var limit int64
var offset int64
var silent bool

func init() {
	flag.StringVar(&from, "from", "", "source file")
	flag.StringVar(&to, "to", "", "destination file")
	flag.Int64Var(&offset, "offset", 0, "offset in source file")
	flag.Int64Var(&limit, "limit", 0, "read limit bytes")
	flag.BoolVar(&silent, "s", false, "disable progress bar")
}

func main() {
	flag.Parse()

	if from == "" || to == "" {
		printUsage()
	}

	fileFrom, err := os.Open(from)
	if err != nil {
		writeError("source file error: %v", err)
	}

	stat, err := fileFrom.Stat()
	if err != nil {
		writeError("source file stat error: %v", err)
	}

	if offset >= stat.Size() {
		writeError("offset is greater than length of source file", nil)
	}

	_, err = fileFrom.Seek(offset, 0)
	if err != nil {
		writeError("source file error: %v", err)
	}

	fileTo, err := os.Create(to)
	if err != nil {
		writeError("destination file error: %v", err)
	}

	var dst io.Writer = fileTo
	var bar *pb.ProgressBar

	if !silent {
		copyLen := stat.Size() - offset

		bar = pb.Full.Start64(copyLen)
		dst = bar.NewProxyWriter(fileTo)
	}

	var src io.Reader = fileFrom
	if limit != 0 {
		src = io.LimitReader(src, limit)
	}

	_, err = io.Copy(dst, src)
	if err != nil {
		writeError("copy error: %v", err)
	}

	_ = fileFrom.Close()
	_ = fileTo.Close()

	if !silent && bar != nil {
		bar.Finish()
	}
}

func printUsage() {
	usage := fmt.Sprintf(
		`usage: %s
	-from string
		source file
	-to string
		destination file
	-offset int64
		offset in source file (default: 0)
	-limit int64
		read limit bytes (default: 0 is no limit)
	-s bool
		disable progress bar (default: false)`,
		os.Args[0],
	)

	writeError(usage, nil)
}

func writeError(format string, err error) {
	if err != nil {
		format = fmt.Sprintf(format, err)
	}

	_, _ = fmt.Fprintln(os.Stderr, format)
	os.Exit(1)
}
