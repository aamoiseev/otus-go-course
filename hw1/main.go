package main

import (
	"fmt"
	"os"

	"github.com/beevik/ntp"
)

const (
	host = "0.pool.ntp.org"
)

func main() {
	now, err := ntp.Time(host)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ntp error: %v", err)
		os.Exit(1)
	}

	fmt.Println(now.Format("15:04:05"))
}
