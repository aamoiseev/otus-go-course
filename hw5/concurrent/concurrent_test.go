package concurrent

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestRun(t *testing.T) {
	i := 0

	task := func() error {
		i++
		return nil
	}

	tasks := []func() error{
		task,
		task,
		task,
	}

	errors, ok := Run(tasks, 0, 0)

	assert.True(t, ok)
	assert.Equal(t, 0, len(errors))
	assert.Equal(t, 3, i)
}

func TestRun_AllConcurrent(t *testing.T) {
	i := 0

	task := func() error {
		i++
		return nil
	}

	tasks := []func() error{
		task,
		task,
		task,
	}

	errors, ok := Run(tasks, len(tasks), 0)

	assert.True(t, ok)
	assert.Equal(t, 0, len(errors))
	assert.Equal(t, 3, i)
}

func TestRun_WithAllErrors_AllConcurrent(t *testing.T) {
	task := func(taskIndex int) func() error {
		return func() error {
			return fmt.Errorf("error in task %d", taskIndex)
		}
	}

	tasks := []func() error{
		task(1),
		task(2),
		task(3),
	}

	errors, ok := Run(tasks, len(tasks), len(tasks))

	assert.False(t, ok)
	assert.Len(t, errors, len(tasks))

	assert.Error(t, errors[0], "error in task 1")
	assert.Error(t, errors[1], "error in task 2")
	assert.Error(t, errors[2], "error in task 3")
}

func TestRun_WithOneError_NoConcurrent(t *testing.T) {
	for n := 0; n < 3; n++ {
		i := 0

		task := func(taskIndex int) func() error {
			return func() error {
				if taskIndex == n {
					return fmt.Errorf("error in task %d", taskIndex)
				}

				i++
				return nil
			}
		}

		tasks := []func() error{
			task(0),
			task(1),
			task(2),
		}

		errors, ok := Run(tasks, 0, 1)

		assert.False(t, ok)
		assert.Len(t, errors, 1)
		assert.Equal(t, n, i)

		assert.Error(t, errors[n], "error in task %d", n)
	}
}

func TestRun_WithOneError_AllConcurrent(t *testing.T) {
	t.Parallel()

	var longTaskDone bool

	longTask := func() error {
		time.Sleep(300 * time.Millisecond)
		longTaskDone = true
		return nil
	}

	taskWithError := func() error {
		return fmt.Errorf("error")
	}

	tasks := []func() error{
		longTask,
		taskWithError,
	}

	errors, ok := Run(tasks, 1, 1)

	assert.False(t, ok)
	assert.Len(t, errors, 1)

	assert.Error(t, errors[1], "error")

	time.Sleep(500 * time.Millisecond)

	assert.True(t, longTaskDone)
}

func TestRun_LongTasks_AllConcurrent(t *testing.T) {
	const n = 5

	tasks := make([]func() error, n)
	resultsCh := make(chan int, n)
	defer close(resultsCh)

	for i := 0; i < n; i++ {
		tasks[i] = func(n int) func() error {
			return func() error {
				d := 600 - (n+1)*100
				time.Sleep(time.Duration(d) * time.Millisecond)
				resultsCh <- n
				return nil
			}
		}(i)
	}

	errors, ok := Run(tasks, len(tasks), 1)

	assert.True(t, ok)
	assert.Len(t, errors, 0)

	time.Sleep(500 * time.Millisecond)

	assert.Len(t, resultsCh, n)

	var results []int
	for result := range resultsCh {
		results = append(results, result)

		if len(results) == n {
			break
		}
	}

	assert.Equal(t, []int{4, 3, 2, 1, 0}, results)
}
