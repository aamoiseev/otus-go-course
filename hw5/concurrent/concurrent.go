package concurrent

type workFunc = func() error

type task struct {
	work  workFunc
	index int
}

type taskResult struct {
	err   error
	index int
}

// Run tasks concurrently.
func Run(tasks []workFunc, maxConcurrent, maxErrors int) (map[int]error, bool) {
	tasksLen := len(tasks)

	if tasksLen == 0 {
		return map[int]error{}, true
	}

	if maxConcurrent < 0 || maxConcurrent > tasksLen {
		maxConcurrent = tasksLen
	}

	if maxErrors < 1 || maxErrors > tasksLen {
		maxErrors = tasksLen
	}

	tasksCh := make(chan task, tasksLen)
	tasksResults := make(chan taskResult, tasksLen)

	wait := make(chan struct{}, maxConcurrent)
	done := make(chan struct{})

	go func() {
		defer close(tasksCh)
		defer close(wait)

		for i, work := range tasks {
			select {
			case <-done:
				return
			case tasksCh <- task{work, i}:
				wait <- struct{}{}
			}
		}
	}()

	for i := 0; i <= maxConcurrent; i++ {
		go func() {
			for task := range tasksCh {
				select {
				case <-done:
					close(tasksResults)
					return
				case tasksResults <- taskResult{task.work(), task.index}:
				}
			}
		}()
	}

	errors := make(map[int]error, len(tasks))

	go func() {
		defer close(done)

		doneCnt := 0

		for res := range tasksResults {
			if res.err != nil {
				errors[res.index] = res.err
			}

			doneCnt++

			if doneCnt == tasksLen || len(errors) == maxErrors {
				return
			}

			<-wait
		}
	}()

	<-done

	return errors, len(errors) == 0
}
