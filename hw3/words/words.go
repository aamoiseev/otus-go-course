package words

import (
	"sort"
	"strings"
	"unicode"
)

// Top returns most frequent words in text.
func Top(text string, n int) []string {
	wordsCnt := make(map[string]int)

	words := strings.FieldsFunc(text, func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	})

	for _, word := range words {
		wordsCnt[strings.ToLower(word)]++
	}

	type wordCnt struct {
		word string
		cnt  int
	}

	freqWordsCnt := make([]wordCnt, 0, len(wordsCnt))
	for word, cnt := range wordsCnt {
		freqWordsCnt = append(freqWordsCnt, wordCnt{
			word,
			cnt,
		})
	}

	sort.Slice(freqWordsCnt, func(i, j int) bool {
		return freqWordsCnt[i].cnt > freqWordsCnt[j].cnt
	})

	if n > len(freqWordsCnt) {
		n = len(freqWordsCnt)
	}

	freqWords := make([]string, 0, n)
	for _, freqWordCnt := range freqWordsCnt[:n] {
		freqWords = append(freqWords, freqWordCnt.word)
	}

	return freqWords
}

// Top10 returns ten frequent words in text.
func Top10(text string) []string {
	return Top(text, 10)
}
