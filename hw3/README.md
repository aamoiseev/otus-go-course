# Домашнее задание №3
**Частотный анализ**

Написать функцию, которая получает на вход текст и возвращает
10 самых часто встречающихся слов без учета словоформ

## Сборка и запуск
`go build -o wfreq`

`curl -s http://www.gutenberg.org/cache/epub/2600/pg2600.txt | gunzip | ./wfreq 100`

Вывод:

```
the
and
to
...
natasha
...
andrew
...
princess
french
...
rostov
has
again
down
moscow
came
come
```

`...` — пропущенные слова в выводе примера
 