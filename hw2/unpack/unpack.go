package unpack

import (
	"strconv"
	"strings"
	"unicode"

	"github.com/pkg/errors"
)

// Do unpacks string.
// For example from "世2界3" to "世世界界界".
// It returns unpacked string or error.
func Do(input string) (string, error) {
	var out strings.Builder

	var skip int
	var currAsChar bool
	var position int
	for i, c := range input {
		position++

		if skip > 0 {
			skip--
			continue
		}

		ch := string(c)
		if currAsChar {
			if ch != `\` && !unicode.IsDigit(c) {
				return "", errors.Errorf(`unexpected \ at position %d`, position-1)
			}

			currAsChar = false
		} else {
			if ch == `\` {
				currAsChar = true
				continue
			}

			if unicode.IsDigit(c) {
				return "", errors.Errorf(`unexpected %v at position %d`, ch, position)
			}
		}

		offset := i + len(ch)
		numLen := countNumLen(input[offset:])

		repeatCount := 1
		if numLen > 0 {
			num := input[offset : offset+numLen]
			numCount, err := strconv.Atoi(num)
			if err != nil {
				return "", errors.Wrapf(err, "failed convert %s to int", num)
			}

			repeatCount = numCount
			skip = numLen
		}

		out.WriteString(strings.Repeat(ch, repeatCount))
	}

	if currAsChar {
		return "", errors.Errorf(`unexpected \ at position %d`, position)
	}

	return out.String(), nil
}

func countNumLen(input string) int {
	numLen := 0
	for _, c := range input {
		if !unicode.IsDigit(c) {
			break
		}
		numLen++
	}

	return numLen
}
