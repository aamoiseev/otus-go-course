package main

import (
	"fmt"
	"os"

	"gitlab.com/aamoiseev/otus-go-course/hw2/unpack"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s [packed-string]\n", os.Args[0])
		os.Exit(1)
	}

	res, err := unpack.Do(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "unpack error: %v", err)
		os.Exit(1)
	}

	fmt.Println(res)
}
